import requests, sys, getopt
from lxml import html
from BeautifulSoup import BeautifulSoup
from term_size import getTerminalSize
    
def make_pretty(to_print): #removes extra whitespace and shit
    to_print = to_print.replace('$$sp', ' ')
    to_print = to_print.replace('  ', ' ')
    to_print = to_print.replace(' ,',',')
    to_print = to_print.replace(' .','.')
    to_print = to_print.replace(' :',':')
    to_print = to_print.replace(' ;',';')
    to_print = to_print.replace(' !','!')
    to_print = to_print.replace(' ?','?')
    to_print = to_print.lstrip()
    to_print = to_print.rstrip(' |')
    to_print = to_print.rstrip()
    to_print = to_print.replace(u'\u2013', '-')
    to_print = to_print.encode('ascii', 'replace')
    return to_print

def print_header(string, weight = 'normal'):
    string = string
    weight = str(weight)
    dashes_number = (getTerminalSize()[0] - len(string) - 3)/2
    string = make_pretty(string)
    
    if weight == "important":
        char = '='
    else:
        char = '-'
    print '\n' + char*dashes_number + ' ' + string + ' ' + char*dashes_number + '\n'
    
def wiki_parse(id, soup):
    attrs={'id': id}
    title_attrs={'id': id+'-h2'}
    target = soup.find('div', attrs)
    target_title = soup.find('h2', title_attrs)
    to_ret = []
    
    to_ret.append(target_title.text)
    
    for ul in target.findAll('ul', recursive = False):
        for li in ul.findAll('li', recursive = False):
            to_add=''
            #'$$sp' is used as a placeholder for the space char because
            #BeautifulSoup fucked things up
            li.replaceWith('$$sp' + li.text + '$$sp')
            for b in li.findAll('b'):
                b.replaceWith('$$sp' + b.text + '$$sp')
            for i in li.findAll('i'):
                i.replaceWith('$$sp' + i.text + '$$sp')
            for span in li.findAll('span'):
                span.replaceWith('$$sp' + span.text + '$$sp')
            for a in li.findAll('a'):
                a.replaceWith('$$sp' + a.text + '$$sp')
            for li_2 in li.findAll('li'):
                li_2.replaceWith('$$sp' + li_2.text + '$$sp|$$sp')
                
            to_add = li.text
            to_add = make_pretty(to_add) + '\n'
            if to_add[:18] != "More anniversaries": #we don't want to print that line
                to_ret.append(to_add)
    return to_ret

def reuters_parse(class_name, soup):
    attrs={'class':class_name}
    target = soup.findAll('div', attrs)
    to_ret = []
    
    for div in target:    
        text = ''
        headline = ''
        link = ''

        for inline in div.findAll('span', {'class':'inlineLinks'}):
            inline.replaceWith('')
        if div.find('h2') != None:
            headline = div.find('h2').text
            link = div.find('h2').find('a', href = True)['href']
            link = 'reuters.com' + link
        if div.find('p') != None and div.find('p') != '':
            text = div.find('p').text
            text.rstrip("Full Article")
            link = '\n' + link
        to_ret.append((headline, make_pretty(text + link) + '\n'))
    return to_ret
        
    
def wiki_print(ext = False):
    url = "http://en.wikipedia.org/"
    
    response = requests.get(url)
    raw_html = response.content
    soup = BeautifulSoup(raw_html, convertEntities=BeautifulSoup.HTML_ENTITIES)

    print_header('WIKIPEDIA', 'important')
    
    parsed_content = wiki_parse('mp-itn', soup)
    print_header(parsed_content[0])
    for to_print in parsed_content[1:]:
        print to_print
        
    if ext:
        parsed_content = wiki_parse('mp-otd', soup)
        print_header(parsed_content[0])
        for to_print in parsed_content[1:]:
            print to_print

def reuters_print(ext = False):
    url = "http://www.reuters.com/"
    
    response = requests.get(url)
    raw_html = response.content
    soup = BeautifulSoup(raw_html, convertEntities=BeautifulSoup.HTML_ENTITIES)

    print_header('REUTERS', 'important')
    parsed_content = reuters_parse('topStory', soup)
    for to_print in parsed_content:
        print_header(to_print[0])
        print to_print[1]
    if ext:
        parsed_content = reuters_parse('feature', soup)
        for to_print in parsed_content:
            print_header(to_print[0])
            print to_print[1]
        
    
#START OF THE SCRIPT

opts, args = getopt.getopt(sys.argv[1:], "hwWrR")
for opt, arg in opts:
    if opt == '-h':
        print '\nNews parser v1.0'
        print '\nUsage:'
        print '\t-w : prints only the "In the news" section from Wikipedia'
        print '\t-W : prints the "In the news" and "On this day..." sections from Wikipedia'
        print '\t-r : prints the top stories from the main Reuters international page'
        print '\t-R : prints top and feature stories from the main Reuters international page'
        print '\t-h : prints this message and quit'
    elif opt == '-w':
        wiki_print()
    elif opt == '-W':
        wiki_print(True)
    elif opt == '-r':
        reuters_print()
    elif opt == '-R':
        reuters_print(True)




    
    




